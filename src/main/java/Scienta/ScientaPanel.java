package Scienta;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import net.miginfocom.swing.MigLayout;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.ITextTarget;

public class ScientaPanel extends AbstractTangoBox {

    private static final long serialVersionUID = 132272895510873887L;

    private JPanel data;
    private JPanel sumData;
    private JPanel step;
    private JPanel detector;
    private JPanel energy;
    private JPanel topLeftPanel;
    private JPanel comboPanel;

    private TextArea statusArea;
    private Label stateLabel;
    private JLabel deviceLabel;

    private StringButton startButton;
    private StringButton stopButton;

    private JLabel excitationEnergyLabel;
    private Label excitationEnergyRead;
    private WheelSwitch excitationEnergyWrite;

    private ComboBox lensModeCombo;
    private ComboBox energyScaleCombo;
    private ComboBox passModeCombo;
    private ComboBox passEnergyCombo;
    private ComboBox acquisitionModeCombo;

    private JLabel xminLabel;
    private WheelSwitch xminWrite;
    private Label xminRead;

    private JLabel xmaxLabel;
    private WheelSwitch xmaxWrite;
    private Label xmaxRead;

    private JLabel yMinLabel;
    private WheelSwitch yminWrite;
    private Label yminRead;

    private JLabel ymaxLabel;
    private WheelSwitch ymaxWrite;
    private Label ymaxRead;

    private JLabel slicesLabel;
    private WheelSwitch slicesWrite;
    private Label slicesRead;

    private JLabel lowLabel;
    private WheelSwitch lowEnergyWrite;
    private Label lowEnergyRead;

    private JLabel fixLabel;
    private WheelSwitch fixEnergyWrite;
    private Label fixEnergyRead;

    private JLabel highLabel;
    private WheelSwitch highEnergyWrite;
    private Label highEnergyRead;

    private JLabel energyStepLabel;
    private Label energyStepRead;
    private WheelSwitch energyStepWrite;

    private JLabel stepTimelabel;
    private WheelSwitch stepTimeWrite;
    private Label stepTimeRead;

    private ImageViewer dataImage;
    private Chart sumDataSpectrum;

    private final static Font wheelFont = new Font("Courier New", Font.PLAIN, 11);
    private final static String VERSION = "0.2.0";

    private final NumberMatrixBox imageBox;
    private final ChartViewerBox chartBox;

    private final ITextTarget acquisitionModeTarget;

    public ScientaPanel() {
        super();
        imageBox = new NumberMatrixBox();
        chartBox = new ChartViewerBox();
        acquisitionModeTarget = new ITextTarget() {
            @Override
            public void setText(String text) {
                setEnergyLabelVisible(text);
            }

            @Override
            public void removeMediator(Mediator<?> mediator) {
            }

            @Override
            public void addMediator(Mediator<?> mediator) {
            }

            @Override
            public String getText() {
                return null;
            }
        };
        initGUI();
    }

    @Override
    protected void onConnectionError() {
        System.out.println("ERROR - onConnectionError");
    }

    @Override
    protected void refreshGUI() {

        deviceLabel.setText(model);

        TangoKey attributeKey;

        TangoKey tangoKey = generateCommandKey("Start");
        setWidgetModel(startButton, stringBox, tangoKey);
        startButton.setText("Start");

        tangoKey = generateCommandKey("Stop");
        setWidgetModel(stopButton, stringBox, tangoKey);
        stopButton.setText("Stop");

        attributeKey = generateAttributeKey("data");
        setWidgetModel(dataImage, imageBox, attributeKey);
        dataImage.setAlwaysFitMaxSize(true);

        attributeKey = generateReadOnlyAttributeKey("energyStep");
        setWidgetModel(energyStepRead, stringBox, attributeKey);
        stringBox.setUnitEnabled(energyStepRead, true);

        attributeKey = generateWriteAttributeKey("energyStep");
        setWidgetModel(energyStepWrite, numberBox, attributeKey);

        attributeKey = generateAttributeKey("state");
        setWidgetModel(stateLabel, stringBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("status");
        setWidgetModel(statusArea, stringBox, attributeKey);
        stringBox.setColorEnabled(statusArea, false);

        attributeKey = generateAttributeKey("mode");
        setWidgetModel(acquisitionModeCombo, stringBox, attributeKey);

        setWidgetModel(acquisitionModeTarget, stringBox, attributeKey);

        attributeKey = generateAttributeKey("energyScale");
        setWidgetModel(energyScaleCombo, stringBox, attributeKey);

        attributeKey = generateAttributeKey("passEnergy");
        setWidgetModel(passEnergyCombo, stringBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("excitationEnergy");
        setWidgetModel(excitationEnergyRead, stringBox, attributeKey);
        stringBox.setUnitEnabled(excitationEnergyRead, true);

        attributeKey = generateWriteAttributeKey("excitationEnergy");
        setWidgetModel(excitationEnergyWrite, numberBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("stepTime");
        setWidgetModel(stepTimeRead, stringBox, attributeKey);
        stringBox.setUnitEnabled(stepTimeRead, true);

        attributeKey = generateWriteAttributeKey("stepTime");
        setWidgetModel(stepTimeWrite, numberBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("lowEnergy");
        setWidgetModel(lowEnergyRead, stringBox, attributeKey);
        stringBox.setUnitEnabled(lowEnergyRead, true);

        attributeKey = generateWriteAttributeKey("lowEnergy");
        setWidgetModel(lowEnergyWrite, numberBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("highEnergy");
        setWidgetModel(highEnergyRead, stringBox, attributeKey);
        stringBox.setUnitEnabled(highEnergyRead, true);

        attributeKey = generateWriteAttributeKey("highEnergy");
        setWidgetModel(highEnergyWrite, numberBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("fixEnergy");
        setWidgetModel(fixEnergyRead, stringBox, attributeKey);

        attributeKey = generateWriteAttributeKey("fixEnergy");
        setWidgetModel(fixEnergyWrite, numberBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("detectorFirstXChannel");
        setWidgetModel(xminRead, stringBox, attributeKey);

        attributeKey = generateWriteAttributeKey("detectorFirstXChannel");
        setWidgetModel(xminWrite, numberBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("detectorLastXChannel");
        setWidgetModel(xmaxRead, stringBox, attributeKey);

        attributeKey = generateWriteAttributeKey("detectorLastXChannel");
        setWidgetModel(xmaxWrite, numberBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("detectorFirstYChannel");
        setWidgetModel(yminRead, stringBox, attributeKey);

        attributeKey = generateWriteAttributeKey("detectorFirstYChannel");
        setWidgetModel(yminWrite, numberBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("detectorLastYChannel");
        setWidgetModel(ymaxRead, stringBox, attributeKey);

        attributeKey = generateWriteAttributeKey("detectorLastYChannel");
        setWidgetModel(ymaxWrite, numberBox, attributeKey);

        attributeKey = generateReadOnlyAttributeKey("detectorSlices");
        setWidgetModel(slicesRead, stringBox, attributeKey);

        attributeKey = generateWriteAttributeKey("detectorSlices");
        setWidgetModel(slicesWrite, numberBox, attributeKey);

        String[] lensModeList = { "lens1", "lens2", "lens3" };
        DeviceProxy tmpDeviceProxy = null;
        try {
            tmpDeviceProxy = new DeviceProxy(model);
            DeviceData dd = tmpDeviceProxy.command_inout("GetLensModeList");
            lensModeList = dd.extractStringArray();
        } catch (DevFailed e) {
            e.printStackTrace();
        }
        lensModeCombo.setValueList((Object[]) lensModeList);
        attributeKey = generateAttributeKey("lensMode");
        setWidgetModel(lensModeCombo, stringBox, attributeKey);

        String[] passModeList = { "pass1", "pass2", "pass3" };
        if (tmpDeviceProxy != null) {
            try {
                DeviceData dd = tmpDeviceProxy.command_inout("GetpassModeList");
                passModeList = dd.extractStringArray();
            } catch (DevFailed e) {
                e.printStackTrace();
            }
        }
        passModeCombo.setValueList((Object[]) passModeList);
        attributeKey = generateAttributeKey("passMode");
        setWidgetModel(passModeCombo, stringBox, attributeKey);

        attributeKey = generateAttributeKey("channelScale");
        TangoKey attributeKey2 = generateAttributeKey("sumData");
        chartBox.connectWidgetDual(sumDataSpectrum, attributeKey, attributeKey2);
        sumDataSpectrum.setAxisName("Energy (eV)", IChartViewer.X);
        sumDataSpectrum.setAxisName("Counts", IChartViewer.Y1);

        setEnergyLabelVisible((String) acquisitionModeCombo.getSelectedItem());

        revalidate();
        repaint();
    }

    @Override
    protected void clearGUI() {
        deviceLabel.setText("");
        cleanWidget(startButton);
        cleanWidget(stopButton);
        cleanWidget(dataImage);
        cleanWidget(energyStepRead);
        cleanWidget(energyStepWrite);
        cleanWidget(stateLabel);
        cleanWidget(statusArea);
        cleanWidget(acquisitionModeCombo);
        cleanWidget(acquisitionModeTarget);
        cleanWidget(energyScaleCombo);
        cleanWidget(passEnergyCombo);
        cleanWidget(excitationEnergyRead);
        cleanWidget(excitationEnergyWrite);
        cleanWidget(stepTimeRead);
        cleanWidget(stepTimeWrite);
        cleanWidget(lowEnergyRead);
        cleanWidget(lowEnergyWrite);
        cleanWidget(highEnergyRead);
        cleanWidget(highEnergyWrite);
        cleanWidget(fixEnergyRead);
        cleanWidget(fixEnergyWrite);
        cleanWidget(xminRead);
        cleanWidget(xminWrite);
        cleanWidget(xmaxRead);
        cleanWidget(xmaxWrite);
        cleanWidget(yminRead);
        cleanWidget(yminWrite);
        cleanWidget(ymaxRead);
        cleanWidget(ymaxWrite);
        cleanWidget(slicesRead);
        cleanWidget(slicesWrite);
        lensModeCombo.setValueList((Object[]) null);
        cleanWidget(lensModeCombo);
        passModeCombo.setValueList((Object[]) null);
        cleanWidget(passModeCombo);
        chartBox.disconnectWidgetFromAll(sumDataSpectrum);
        revalidate();
        repaint();
    }

    private void initGUI() {

        initTopLeftPanel();

        // Sum data
        initChartViewer();

        JPanel topPanel = new JPanel(new BorderLayout());
        topPanel.add(topLeftPanel, BorderLayout.WEST);
        topPanel.add(sumData, BorderLayout.CENTER);

        initComboPanel();

        // Energy
        initEnergyPanel();

        // Step
        initStepPanel();

        // Detector
        initDetectorPanel();

        Box paramsBox = Box.createVerticalBox();
        paramsBox.add(comboPanel);
        paramsBox.add(energy);
        paramsBox.add(step);
        paramsBox.add(detector);

        // prevent this panel from getting extra space
        comboPanel.setMaximumSize(comboPanel.getPreferredSize());

        // Data
        initImageViewer();

        setLayout(new BorderLayout());

        this.add(topPanel, BorderLayout.NORTH);
        this.add(paramsBox, BorderLayout.WEST);
        this.add(data, BorderLayout.CENTER);

        this.setPreferredSize(new java.awt.Dimension(820, 800));
    }

    private void initChartViewer() {
        sumData = new JPanel();
        sumData.setLayout(new BorderLayout());
        sumData.setBorder(BorderFactory.createTitledBorder("Sum Data"));

        sumDataSpectrum = new Chart();
        sumDataSpectrum.setManagementPanelVisible(false);
        sumData.add(sumDataSpectrum, BorderLayout.CENTER);

        sumDataSpectrum.setPreferredSize(new java.awt.Dimension(560, 250));
    }

    private void initImageViewer() {
        data = new JPanel();
        data.setLayout(new BorderLayout());
        data.setBorder(BorderFactory.createTitledBorder("Data"));

        dataImage = new ScientaImageViewer();
        dataImage.setToolBarVisible(true);
        data.add(dataImage, BorderLayout.CENTER);
    }

    private void initTopLeftPanel() {
        topLeftPanel = new JPanel();

        // State
        stateLabel = generateLabel();
        stateLabel.setPreferredSize(new Dimension(60, stateLabel.getPreferredSize().height));

        deviceLabel = new JLabel();

        // Status
        statusArea = new TextArea();
        statusArea.setBorder(BorderFactory.createTitledBorder("Status"));

        // Start
        startButton = new StringButton();
        // Stop
        stopButton = new StringButton();

        // excitationEnergy
        // Label
        excitationEnergyLabel = new JLabel();
        excitationEnergyLabel.setText("Excitation Energy");
        // Setter
        excitationEnergyWrite = generateWheelSwitch();
        // Viewer
        excitationEnergyRead = generateLabel();
        excitationEnergyRead.setMaximumSize(new Dimension(85, 28));

        // Lens mode
        lensModeCombo = new ComboBox();
        lensModeCombo.setBorder(BorderFactory.createTitledBorder("Lens Mode"));
        // lensModeCombo.setPrototypeDisplayValue("0123456789");

        Box deviceBox = Box.createHorizontalBox();
        deviceBox.add(stateLabel);
        deviceBox.add(Box.createRigidArea(new Dimension(5, 0)));
        deviceBox.add(deviceLabel);
        deviceBox.add(Box.createHorizontalGlue());

        Box buttonBox = Box.createHorizontalBox();
        buttonBox.add(Box.createHorizontalGlue());
        buttonBox.add(startButton);
        buttonBox.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonBox.add(Box.createHorizontalGlue());
        buttonBox.add(stopButton);
        buttonBox.add(Box.createHorizontalGlue());

        Box excitationBox = Box.createHorizontalBox();
        excitationBox.add(Box.createHorizontalGlue());
        excitationBox.add(excitationEnergyWrite);
        excitationBox.add(Box.createRigidArea(new Dimension(5, 0)));
        excitationBox.add(excitationEnergyRead);
        excitationBox.add(Box.createHorizontalGlue());
        excitationBox.setPreferredSize(new Dimension(excitationBox.getPreferredSize().width, 28));

        topLeftPanel.setLayout(new BoxLayout(topLeftPanel, BoxLayout.Y_AXIS));
        excitationEnergyLabel.setAlignmentX(CENTER_ALIGNMENT);

        topLeftPanel.add(deviceBox);
        topLeftPanel.add(Box.createVerticalStrut(5));
        topLeftPanel.add(statusArea);
        topLeftPanel.add(Box.createVerticalStrut(5));
        topLeftPanel.add(buttonBox);
        topLeftPanel.add(Box.createVerticalStrut(10));
        topLeftPanel.add(excitationEnergyLabel);
        topLeftPanel.add(excitationBox);
        topLeftPanel.add(lensModeCombo);

    }

    private void initComboPanel() {
        // EnergyScale
        energyScaleCombo = new ComboBox();
        energyScaleCombo.setValueList("Kinetic", "Binding");
        energyScaleCombo.setBorder(BorderFactory.createTitledBorder("Energy Scale"));

        // Pass Mode
        passModeCombo = new ComboBox();
        passModeCombo.setBorder(BorderFactory.createTitledBorder("Pass Mode"));

        // Pass Energy
        passEnergyCombo = new ComboBox();
        passEnergyCombo.setValueList("1", "2", "5", "10", "20", "50", "100", "200", "500");
        passEnergyCombo.setBorder(BorderFactory.createTitledBorder("Pass Energy"));

        // Acquisition Mode
        acquisitionModeCombo = new ComboBox();
        acquisitionModeCombo.setValueList("Fixed", "Swept");
        acquisitionModeCombo.setBorder(BorderFactory.createTitledBorder("Acquisition Mode"));

        comboPanel = new JPanel();
        comboPanel.setLayout(new MigLayout("insets 0, wrap 2", "[fill, 105::][fill, 135::]", ""));

        comboPanel.add(energyScaleCombo);
        comboPanel.add(passModeCombo);
        comboPanel.add(passEnergyCombo);
        comboPanel.add(acquisitionModeCombo);
    }

    private void initEnergyPanel() {
        // Low
        lowLabel = new JLabel();
        lowLabel.setText("Low");

        lowEnergyWrite = generateWheelSwitch();

        lowEnergyRead = generateLabel();
        lowEnergyRead.setText("low");

        // Fix
        fixLabel = new JLabel();
        fixLabel.setText("Fix");

        fixEnergyWrite = generateWheelSwitch();

        fixEnergyRead = generateLabel();
        fixEnergyRead.setText("fix");

        // High
        highLabel = new JLabel();
        highLabel.setText("High");

        highEnergyWrite = generateWheelSwitch();

        highEnergyRead = generateLabel();
        highEnergyRead.setText("high");

        energy = new JPanel();
        energy.setBorder(BorderFactory.createTitledBorder("Energy"));

        energy.setLayout(new MigLayout("insets 0 n, wrap 3", "[left]unrel:push[right]rel[center, fill, 85::]",
                "[28!]0:push"));

        energy.add(lowLabel);
        energy.add(lowEnergyWrite);
        energy.add(lowEnergyRead);

        energy.add(fixLabel);
        energy.add(fixEnergyWrite);
        energy.add(fixEnergyRead);

        energy.add(highLabel);
        energy.add(highEnergyWrite);
        energy.add(highEnergyRead);

    }

    private void initStepPanel() {

        // Energy Step
        energyStepLabel = new JLabel();
        energyStepLabel.setText("Energy Step");

        energyStepWrite = generateWheelSwitch();

        energyStepRead = generateLabel();

        // Step time
        stepTimelabel = new JLabel();
        stepTimelabel.setText("Step Time");

        stepTimeWrite = generateWheelSwitch();

        stepTimeRead = generateLabel();

        step = new JPanel();
        step.setBorder(BorderFactory.createTitledBorder("Step"));

        MigLayout layout = new MigLayout("insets 0 n", // Layout Constraints
                "[right]rel[center, fill, 85::]", // Column constraints
                "[][28!][][28!]"); // Row constraints

        step.setLayout(layout);

        step.add(energyStepLabel, "span 2, left, wrap 2");
        step.add(energyStepWrite, "push");
        step.add(energyStepRead, "wrap push");

        step.add(stepTimelabel, "span 2, left, wrap 2");
        step.add(stepTimeWrite, "push");
        step.add(stepTimeRead);
    }

    private void initDetectorPanel() {
        // Xmin
        xminLabel = new JLabel();
        xminLabel.setText("X min");

        xminWrite = generateWheelSwitch();

        xminRead = generateLabel();
        xminRead.setText("xminRead");

        // Xmax
        xmaxLabel = new JLabel();
        xmaxLabel.setText("X max");

        xmaxWrite = generateWheelSwitch();

        xmaxRead = generateLabel();
        xmaxRead.setText("xmaxRead");

        // Ymin
        yMinLabel = new JLabel();
        yMinLabel.setText("Y min");

        yminWrite = generateWheelSwitch();

        yminRead = generateLabel();
        yminRead.setText("yminRead");

        // Ymax
        ymaxLabel = new JLabel();
        ymaxLabel.setText("Y max");

        ymaxWrite = generateWheelSwitch();

        ymaxRead = generateLabel();
        ymaxRead.setText("ymaxRead");

        // Slice
        slicesLabel = new JLabel();
        slicesLabel.setText("Slices");

        slicesWrite = generateWheelSwitch();

        slicesRead = generateLabel();
        slicesRead.setText("slicesRead");

        detector = new JPanel();
        detector.setBorder(BorderFactory.createTitledBorder("Detector"));

        MigLayout layout = new MigLayout("insets 0 n, wrap 3", // Layout Constraints
                "[left]unrel:push[right]rel[center, fill, 85::]", // Column constraints
                "[28!]0:push"); // Row constraints

        detector.setLayout(layout);

        detector.add(xminLabel);
        detector.add(xminWrite);
        detector.add(xminRead);

        detector.add(xmaxLabel);
        detector.add(xmaxWrite);
        detector.add(xmaxRead);

        detector.add(yMinLabel);
        detector.add(yminWrite);
        detector.add(yminRead);

        detector.add(ymaxLabel);
        detector.add(ymaxWrite);
        detector.add(ymaxRead);

        detector.add(slicesLabel);
        detector.add(slicesWrite);
        detector.add(slicesRead);

    }

    @Override
    protected WheelSwitch generateWheelSwitch() {
        WheelSwitch wheelSwitch = super.generateWheelSwitch();
        wheelSwitch.setOpaque(false);
        wheelSwitch.setFont(wheelFont);
        return wheelSwitch;
    }

    private void setEnergyLabelVisible(String acquisitionMode) {
        // TODO replace this with a card layout to reduce panel size (one row saved)
        if (acquisitionMode.equalsIgnoreCase("Swept")) {
            lowLabel.setVisible(true);
            lowEnergyRead.setVisible(true);
            lowEnergyWrite.setVisible(true);

            highLabel.setVisible(true);
            highEnergyRead.setVisible(true);
            highEnergyWrite.setVisible(true);

            fixLabel.setVisible(false);
            fixEnergyRead.setVisible(false);
            fixEnergyWrite.setVisible(false);
        } else {
            lowLabel.setVisible(false);
            lowEnergyRead.setVisible(false);
            lowEnergyWrite.setVisible(false);

            highLabel.setVisible(false);
            highEnergyRead.setVisible(false);
            highEnergyWrite.setVisible(false);

            fixLabel.setVisible(true);
            fixEnergyRead.setVisible(true);
            fixEnergyWrite.setVisible(true);
        }
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // Not managed
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // Not managed
    }

    /**
     * Auto-generated main method to display this JPanel inside a new JFrame.
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        ScientaPanel panel = new ScientaPanel();
        String model = "";
        if ((args != null) && (args.length > 0)) {
            model = args[0];
            try {
                panel.setModel(model);
                panel.start();
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
        frame.setTitle("Zouk " + VERSION + " : " + model);
        frame.setLocation(new Point(250, 50));
        frame.getContentPane().add(panel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
