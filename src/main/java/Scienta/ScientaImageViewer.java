package Scienta;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.util.ij.roi.LineProfileRoiGenerator;
import fr.soleil.comete.swing.image.util.ij.roi.SelectionRoiGenerator;
import fr.soleil.comete.swing.image.util.ij.roi.ZoomRoiGenerator;

public class ScientaImageViewer extends ImageViewer {

    private static final long serialVersionUID = 821030066449282818L;

    public ScientaImageViewer() {
        super();
    }

    public ScientaImageViewer(IJRoiManager imgp) {
        super(imgp);
    }

    @Override
    protected void registerRoiGenerators() {
        registerRoiGenerator(new SelectionRoiGenerator(null,
                MESSAGES.getString("ImageJViewer.Action.SelectionMode.Tooltip")));
        registerRoiGenerator(new ZoomRoiGenerator(null, MESSAGES.getString("ImageJViewer.Action.ZoomMode.Tooltip")));
        registerRoiGenerator(new LineProfileRoiGenerator(
                MESSAGES.getString("ImageJViewer.Action.LineProfileMode.Text"),
                MESSAGES.getString("ImageJViewer.Action.LineProfileMode.Tooltip"), this));
    }

}
